package com.kachyng.mobile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

/**
 * Created by raghavakumarburugadda on 04/12/16.
 */

public abstract class BaseActivity extends AppCompatActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.left_arrow);
        TextView toolbar_title=(TextView)findViewById(R.id.toolbar_title);
        toolbar_title.setText(getToolbarTitle());
    }
    public abstract int getLayoutResource();
    public abstract String getToolbarTitle();
}
