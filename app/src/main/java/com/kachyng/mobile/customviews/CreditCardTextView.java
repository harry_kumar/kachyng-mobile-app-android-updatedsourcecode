package com.kachyng.mobile.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by raghavakumarburugadda on 26/12/16.
 */

public class CreditCardTextView extends TextView{
    public CreditCardTextView(Context context) {
        super(context);
        init(context);
    }

    public CreditCardTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CreditCardTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public CreditCardTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }
    public void init(Context context)
    {
        Typeface typeface=Typeface.createFromAsset(context.getAssets(),"ocra.otf");
        setTypeface(typeface);
    }
}
