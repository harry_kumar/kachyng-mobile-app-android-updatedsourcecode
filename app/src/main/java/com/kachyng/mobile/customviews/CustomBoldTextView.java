package com.kachyng.mobile.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by raghavakumarburugadda on 26/12/16.
 */

public class CustomBoldTextView extends TextView{
    public CustomBoldTextView(Context context) {
        super(context);
        initView(context);
    }

    public CustomBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public CustomBoldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    public CustomBoldTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context);
    }
    public void initView(Context context)
    {
        Typeface typeface=Typeface.createFromAsset(context.getAssets(),"helvetica_bold.ttf");
        setTypeface(typeface);
    }
}
